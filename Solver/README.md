Due to limitations in KUKA.Sim, numpy can not be installed into KUKA.Sim's python library. Hence,
there are two versions of the solver, one that uses numpy (specifically to enable a more advanced
solver, Backward Euler) and one which is usable in KUKA.Sim that does not use numpy. Only the 
Euler solver is available in the KUKA.Sim version.