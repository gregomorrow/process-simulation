import Solver as S
import matplotlib.pyplot as pyplot
import BouncingBall as model

start_height = 10.0
start_velocity = 0.0
x0 = [start_height,start_velocity]

solver_dt = 0.01

s = S.Solver(x0,[0],0,solver_dt,model)

res = s.Simulate(10)

pyplot.plot(res.T, [y[0] for y in res.Y])
pyplot.show()
