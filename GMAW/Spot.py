# Spot welding model
# "An Electrochemical Model Based Adaptive Control of Resistance Spot Welding."
# Ziyad Kas, Manohar Das
import math
import Solver as S

l_1 = .0005 # (m) distance from melting interface to electrode contact surface
p = 0.0005 # (m) nugget penetration
rho_0 = 1.59e-7 # (ohm*m) resistivity at reference temperature (32C)
theta_0 = 300 # (K) reference temperature
alpha_r = 10 # temperature coefficient
H = 2.47e5 # (J/kg) heat of fusion per unit volume
a = 0.0005 # (m) nugget radius
rho = 7.87e3 # (kg/m^3) desity
C_p = 4.86e2 # (J/kg*C) specific heat
k_1 = 51.9 # (W/m*K) thermal conductivity
pi = math.pi
beta = 0.5 # final penetration to workpiece ratio
L = 0.001 # (m) workpiece thickness
b = 0.0005 # (m) electrode radius
theta_1 = 1673 # (K) melting temperature
A = pi*b*b # (m^2) cross-sectional area

c1 = 2*l_1*rho_0*alpha_r/A
c2 = 2*l_1*rho_0*(1-alpha_r*theta_0)/A
c3 = 2*p*rho_0*alpha_r/A
c4 = 2*p*rho_0*(1-alpha_r*theta_0)/A
c5 = c1 + c3
c6 = c2 + c4
c7 = H*pi*a*a*p
c8 = rho*C_p*pi*a*a*p
c9 = (k_1*pi*a*a/l_1 + 10*k_1*pi*a*a*beta*L/b*math.sqrt(alpha_r))
c10 = k_1*pi*a*a*theta_1/l_1
c11 = c5/c8
c12 = c6/c8
c13 = c9/c8
c14 = theta_0*c13 # (c10-c7)/c8
#print "c13 = {}\tc14 = {}".format(c13,c14)

def f(x,u,t):
    return [c11*u[0]*x[0]+c12*u[0]-c13*x[0]+c14]

def g(x,u,t):
    return [x[0], 0]

# no reset
def e(x,u,t):
    return S.ResetResult(False, None)

def jac(x,u,t):
    return [c11*u[0]-c13]
