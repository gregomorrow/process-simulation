class PI_Controller:
    def __init__(self, k_P, k_I, upper_lim, lower_lim):
        self.k_P = k_P
        self.k_I = k_I
        self.I = 0
        self.u = upper_lim
        self.l = lower_lim
        self.t_prev = 0
        self.k_sat = -0.1

    # perform one step of PI control
    def control(self, process, setpoint, t):
        dt = t - self.t_prev
        self.t_prev = t
        d = setpoint-process
        self.I += dt*d
        val = self.k_P*d + self.k_I*self.I
        ud = val - self.u
        if (ud > 0):
            # upper saturation
            out = self.u
            self.I += self.k_sat*ud
        else:
            ld = val - self.l
            if (ld < 0):
                # lower saturation
                out = self.l
                self.I += self.k_sat*ld
            else:
                out = val
        return out
