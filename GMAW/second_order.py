# a first order system
import Solver as S

def f(x,u,t):
    k1 = -1
    k12 = 0.5
    k2 = -1.3
    k22 = 0.2
    return [k1*x[0] + k12*x[1] , k22*x[0]+ k2*x[1] + u[0]]

def g(x,u,t):
    return [x[0],x[1],u[0]]

def e(x,u,t):
    return S.ResetResult(False, [])

def jac(x,u,t):
    pass
