# finite difference computation of spot welding process
# see A ONE-DIMENSIONAL SPOT WELDING MODEL
# K. T. ANDREWS, L. GUESSOUS, S. NASSAR, S. V. PUTTA, AND M. SHILLOR
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from Spot1D_parameters import Steel as Steel

N = 300             # total number of cells
n1 = 150            # number of cells in first plate
l1 = .002           # (m) thickness of first plate
l2 = .002           # (m) thickness of second plate
L = l1 + l2         # (m) total thickness
dx1 = l1/(n1-1)     # (m) x increment in first plate
dx2 = l2/(N-n1)     # (m) x increment in second plate
T_melt = 1450.0       # (K) melting temp (TODO need for each plate)
T_ambient = 300.0     # (K) ambient temperature
lambda_1 = 2.6e5    # (J) heat of fusion in first plate
lambda_2 = 2.6e5    # (J) heat of fusion in second plate
dt = 0.0002         # (sec) time step
t_final = 10*dt        # (sec) final time
R_0 = 1e-7          # (ohm) external circuit resistance
R_g_0 = 75.0          # (ohm) gap resistance at ambient temp
k_R_g = -(R_g_0/(T_melt-T_ambient))        # (ohm/K) gap resistance linear constant
V_0 = 2e4           # (V) open circuit voltage
t_V_end = .255       # time to cut off the voltage
rho_s = 8.06e3      # (kg/m^3) solid density (at T_melt)
rho_l = 7.0e3       # (kg/m^3) liquid density (at T_melt)
saveTime = dt      # how often to save states
saveTimeMin = 0.0   # min time to save a frame
saveRate = int(saveTime/dt)      # save off the T's every "saveRate" steps
saveMin = int(saveTimeMin/dt)    # don't save any frames until this step
saveFig = True      # save the figure to disk?

# model a physical property's variation with temperature,
# assuming a linear form on either side of the melting point,
# and ionterpolating linearly using beta at the melt temp
def physicalProperty(T,beta,T_critical,property_solid,slope_solid,property_liquid,slope_liquid):
    dT = T-T_critical
    if (dT < 0):
        # solid
        return property_solid + dT*slope_solid
    if (dT > 0):
        # liquid
        return property_liquid + dT*slope_liquid
    # mushy zone
    return beta*property_liquid + (1-beta)*property_solid

# specific heat
def C(T,beta):
    c_s = 681.0 # (J/kg K)
    slope_s = 0.0
    c_l = 753.0 # (J/kg K)
    slope_l = 0.0
    return physicalProperty(T, beta, T_melt, c_s, slope_s, c_l, slope_l)

# thermal difusivity
def K(T, beta):
    k_s = 1.1e0
    slope_s = 0
    k_l = 0.9e0
    slope_l = 0
    return physicalProperty(T, beta, T_melt, k_s, slope_s, k_l, slope_l)

def Sigma(T, beta):
    sigma_s = 10.0
    slope_s = 0
    sigma_l = 10.0
    slope_l = 0
    return physicalProperty(T, beta, T_melt, sigma_s, slope_s, sigma_l, slope_l)

# what is the temperature at the given enthalpy?
# we are really just trying to invert the enthalpy as a function of temperature (eqn 2.1)
def T_func(H):
    c_melt_s = C(T_melt, 0)
    c_melt_l = C(T_melt, 1)
    if (H < rho_s*c_melt_s*T_melt): # below the melt temp--note we are treating c as constant!
        return H/(rho_s*c_melt_s)
    if (H > rho_l*(c_melt_s*T_melt + lambda_1)): # above the melt temperature
        Lambda = lambda_1 + (c_melt_s-c_melt_l)*T_melt
        return ((H/rho_l)-Lambda)/c_melt_l
    return T_melt

# the gap resistance. modeled as linearly decreasing up to T_melt, then zero thereafter
def R_g_func(T, R_cur):
    if (R_cur == 0 or T >= T_melt):
        return 0
    # R_cur is not zero and T < T_melt
    return R_g_0+ k_R_g*(T-T_ambient)


# see eqn 3.4
def R_plates(s, R_g):
    R1 = 0
    R2 = 0
    for j in range(0,n1):
        R1 += (1/s[j] + 1/s[j+1])
    R1 *= (dx1/2)
    for j in range(n1+1,N-1):
        R2 += (1/s[j] + 1/s[j+1])
    R2 *= (dx2/2)
    return R1 + R2 + R_g

# see eqn 3.5
def T_interface(T,k,R_g,I):
    dk = (k[n1-1]/dx1 + k[n1+1]/dx2)
    return (T[n1-1]*k[n1-1]/dx1 + I*I*R_g + T[n1+1]*k[n1+1]/dx2)/dk

def T_ave(T):
    sum = 0
    for t in T:
        sum += t
    return sum/len(T)

# solution steps
# 1. given the enthalpies at time i, compute the non-interface enthalpies at time i+1 (eqns 3.2, 2.1, 3.3, 3.4)
# 2. evalutate the interface temperature using eqn 3.5. For R_g, use a linear model that goes to zero when the
#    interface reaches the melting temp.
# 3. given the temperature at all points at time i+1, go through each position, and:
#   a. note if it is at/above the melt temp (to keep track of nugget size)
#   b. if it is at the melting temp, compute the liquid fraction from eqn 3.6
#   c. compute the phsical constants (k,c,sigma) at each position (for next iteration)

# the following variables are updated each time step
t = 0
H = [0]*N       # enthalpies
T = [0]*N       # temperatures
maxT = [0]*N    # max temperatures
c = [0]*N       # specific heats
k = [0]*N       # diffusivities
x = [0]*N       # node positions
sigma = [0]*N   # resistivities
nugget = [0]*N  # has this cell ever been melted?
beta = [0]*N    # liquid fraction in this cell
R_g = R_g_func(T_ambient, R_g_0)         # gap resistance
I = 0           # current


# set up initial conditions
for i in range(N):
    T[i] = T_ambient
    c[i] = C(T[i], 0)
    H[i] = rho_s*c[i]*T[i] # this assumes solid form at the beginning
    k[i] = K(T[i], 0)
    sigma[i] = Sigma(T[i], 0)
    if (i <= n1): # first plate
        x[i] = i*dx1
    else:  # second plate
        x[i] = n1*dx1 + (i-n1)*dx2

z = [0]*N
Ts = [] #[z]*100       # temperature history
step = 0
saveIndex = 0
file = open("temperatures.csv", "w")
file3 = open("beta.csv", "w")

while t < t_final:
    # 1. given the enthalpies at time i, compute the non-interface enthalpies at time i+1 (eqns 3.2, 2.1, 3.3, 3.4)
    # get the interface resistance and total resistance, and compute the current
    R_g = R_g_func(T[n1], R_g)
    R_p = R_plates(sigma,R_g)

    R_tot = R_0 + R_p
    I = V_0/R_tot


    # turn off the current after t_V_end
    if (t > t_V_end):
        I = 0
    print "Rtot = {}, R_0= {}, R_p = {}, R_g = {}".format(R_tot, R_0, R_p, R_g)

    # save temperature history
    if (step%saveRate == 0 and step >= saveMin):
        #Ts[saveIndex] = T
        #Ts.append(T)
        #saveIndex += 1
        file.write(str(T)+"\n")
        file3.write(str(beta)+"\n")
        #print "[{}] {}".format(t, T)

    # now advance the enthalpy and temperature everywhere except interface
    for j in range (1,n1): # portion in plate 1
        F = I*V_0/(sigma[j])
        dH = dt*((1/(dx1*dx1))*((k[j]+k[j+1])*(T[j+1]-T[j])-(k[j-1]+k[j])*(T[j]-T[j-1]))+F)
        if (j==n1-1 and step%saveRate==0):
            print "[{}] H = {}, dH = {}, T = {}, beta = {}, F = {}, I = {}".format(t, H[j], dH, T[j], beta[j], F,I)
            #print("H = {} dH = {} T = {}".format(H[j], dH, T[j]))
        H[j] += dH
        T[j] = T_func(H[j])
    for j in range (n1+1,N-1): # portion in plate 2
        F = I*V_0/sigma[j]
        H[j] += dt*(1/(dx2*dx2)*((k[j]+k[j+1])*(T[j+1]-T[j])-(k[j-1]+k[j])*(T[j]-T[j-1]))+F)
        T[j] = T_func(H[j])

    # 2. evalutate the interface temperature using eqn 3.5.
    T[n1] = T_interface(T, k, R_g, I)

    # 3. given the temperature at all points at time i+1, go through each position, and:
    c_melt_s = C(T_melt, 0)
    for j in range (0,N):
        #   a. note if it is at/above the melt temp (to keep track of nugget size)
        #   b. if it is at the melting temp, compute the liquid fraction from eqn 3.6
        if (T[j] > T_melt):
            nugget[j] = 1
            beta[j] = 1
        if (T[j] == T_melt):
            # compute the liquid fraction
            beta[j] = (H[j]/rho_s - c_melt_s*T_melt)/(lambda_1-H[j]*(1/rho_l-1/rho_s))
            if (beta[j] > 1):
                beta[j] = 1
            if (beta[j] < 0):
                beta[j] = 0
        if (T[j] < T_melt):
            beta[j] = 0
        # and update the physical parameters at this point
        c[j] = C(T[j], beta[j])
        k[j] = K(T[j], beta[j])
        sigma[j] = Sigma(T[j], beta[j])

        # update the max temps
        if (beta[j] > maxT[j]):
            maxT[j] = beta[j]
    t += dt
    step += 1


file.close()
file3.close()

# write out the final nugget information
file2 = open("nugget.csv","w")
file2.write(str(nugget)+"\n")
file2.close()

# write out the max temperature information
file2 = open("max_temps.csv","w")
file2.write(str(maxT)+"\n")
file2.close()


def readDataFromFile(filename):
    file = open(filename,"r")
    l = file.readline()
    Data = []
    while l != "":
        nl = l.strip()
        nl = nl[1:len(nl)-1]
        ls = nl.split(",")
        a = []
        for l in ls:
            a.append(float(l))
        l = file.readline()
        Data.append(a)
    file.close()
    return Data

Temps = readDataFromFile("temperatures.csv")
Nugget = readDataFromFile("nugget.csv")
Beta = readDataFromFile("beta.csv")

xx = range(len(Beta[0]))
#fig, ax = plt.subplots()
#ax.autoscale()
xdata, ydata = [], []
#ln, = plt.plot(xx, Beta[0], animated=True)

def init():
    ax.set_xlim(0, len(Beta[0]))
    ax.set_ylim(0,1.3)
    return ln,

def update(frame):
    #print "Update {}".format(frame)
    #ln.set_data(x, Ts[frame])
    #x = range(100)
    #y = []
    #for xx in x:
    #    y.append(frame*xx)
    ln.set_label(frame)
    ln.set_ydata(Beta[frame])
    return ln,

#ani = FuncAnimation(fig, update, frames=range(len(Beta)), init_func=init, blit=True, interval = 1)
#plt.show()

z = [0]*N # the zero line for plotting
title = "V = {} kV; pulse duration = {} ms".format(V_0/1000, t_V_end*1000)
f,ax = plt.subplots(1, figsize=[7,5])
f.suptitle(title)
ax.axvline(x=l1, color="black")
ax.set_ylabel("Temperature history (K)")
ax.set_xlabel("Position on workpiece (m)")
ax.plot(x,map(list,zip(*Temps)))
ax.fill_between(x,z,[m*200 for m in maxT], facecolor = 'orange')
ax.fill_between(x,z,[n*200 for n in nugget], facecolor = 'red')
#ax[2].axvline(x=l1, color="black")
if (saveFig):
    plt.savefig(title + ".png")
plt.show()
