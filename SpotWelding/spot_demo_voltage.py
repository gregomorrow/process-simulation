import Spot1D_parameters as Parameters
import Spot1D_n as Spot
import matplotlib.pyplot as P

class NuggetResult:
    def __init__(self,weld_times,nugget_sizes,thickness,voltage):
        self.nugget_sizes = nugget_sizes
        self.weld_times = weld_times
        self.thickness = thickness
        self.voltage = voltage

solverParameters = Parameters.Spot1D_Solver_Parameters() # just take defaults
physicalParameters = Parameters.Steel

#factor = 1
solverParameters.t_final = 0.3

#solverParameters.setSaveInfo(solverParameters.dt*factor)

#print "phys param {}".format(physicalParameters.V_0)

physicalParameters.V_0 = 17.5e3
thickness = 0.003
voltages = [17.5e3, 20.0e3, 22.5e3]
weld_time_delta_t = 0.002
num_welds = [15, 15, 15]
weld_times = [0.12, 0.08, 0.06]

n_result = []

for i in range(len(voltages)):
    voltage = voltages[i]
    physicalParameters.set_l1(thickness)
    physicalParameters.V_0 = voltage
    nugget_sizes = []
    times = []
    weld_time = weld_times[i]
    for j in range(num_welds[i]):
        t_end = weld_time + j*weld_time_delta_t
        physicalParameters.t_V_end = t_end
        s = Spot.Spot1D_Solver(physicalParameters, solverParameters)
        done = False

        while not done:
            if (s.t < physicalParameters.t_V_end):
                u = [physicalParameters.V_0]
            else:
                u = [0]
            done = s.advance(u)
        s.finalize()
        n = s.getNuggetExtent()
        if (n>=1.4e-5):
            nugget_sizes.append(n)
            times.append(t_end)
        print "[{}]: Nugget extent = {}".format(t_end, n)
    nr = NuggetResult(times, nugget_sizes, thickness, voltage)
    n_result.append(nr)

    #s.createPlot()
f,ax = P.subplots()
for nr in n_result:
    ax.plot([w*1000 for w in nr.weld_times], [n*1000 for n in nr.nugget_sizes],"o-", label = "V = {}kV".format(nr.voltage/1000))
title = "Nugget size versus spot weld duration; l1 = {} mm; l2 = {} mm".format(physicalParameters.l1*1000,physicalParameters.l2*1000)
ax.legend()
P.suptitle(title)
P.ylabel("nugget size (mm)")
P.xlabel("weld duration (ms)")
P.savefig(title+".png")
P.show()
