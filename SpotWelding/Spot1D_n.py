# finite difference computation of spot welding process
# see A ONE-DIMENSIONAL SPOT WELDING MODEL
# K. T. ANDREWS, L. GUESSOUS, S. NASSAR, S. V. PUTTA, AND M. SHILLOR
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import Spot1D_parameters as P

class Spot1D_Solver:
    def __init__(self, physical_parameters, solver_parameters):
        self.P = physical_parameters
        self.S = solver_parameters

# The following variables are updated each time step. They are lists because
# the values must be tracked at each position of the workpiece.
        N = self.P.N
        self.t = 0.0           # simulation time
        self.H = [0.0]*N       # enthalpies
        self.T = [0.0]*N       # temperatures
        self.maxT = [0.0]*N    # max temperatures
        self.c = [0.0]*N       # specific heats
        self.k = [0.0]*N       # diffusivities
        self.x = [0.0]*N       # node positions
        self.sigma = [0.0]*N   # resistivities
        self.nugget = [0.0]*N  # has this cell ever been melted?
        self.beta = [0.0]*N    # liquid fraction in this cell
        self.R_g = self.R_g_func(self.P.T_ambient, self.P.R_g_0)         # gap resistance
        self.I = 0.0           # current


        # set up initial conditions
        for i in range(self.P.N):
            self.T[i] = self.P.T_ambient
            self.c[i] = self.P.C.Value(self.T[i], 0)
            self.H[i] = self.P.rho_s*self.c[i]*self.T[i] # this assumes solid form at the beginning
            self.k[i] = self.P.K.Value(self.T[i], 0)
            self.sigma[i] = self.P.Sigma.Value(self.T[i], 0)
            if (i <= self.P.n1): # first plate
                self.x[i] = i*self.P.dx1
            else:  # second plate
                self.x[i] = self.P.n1*self.P.dx1 + (i-self.P.n1)*self.P.dx2

        self.Ts = [self.T]
        self.z = [0]*N
        self.step = 0
        self.saveIndex = 0
        self.file = open("temperatures.csv", "w")
        self.file3 = open("beta.csv", "w")


    # what is the temperature at the given enthalpy?
    # we are really just trying to invert the enthalpy as a function of temperature (eqn 2.1)
    def T_func(self, H):
        T_melt = self.P.T_melt
        rho_s = self.P.rho_s
        rho_l = self.P.rho_l
        lam = self.P.lambda_

        c_melt_s = self.P.C.Value(T_melt, 0)
        c_melt_l = self.P.C.Value(T_melt, 1)

        if (H < rho_s*c_melt_s*T_melt): # below the melt temp--note we are treating c as constant!
            return H/(rho_s*c_melt_s)
        if (H > rho_l*(c_melt_s*T_melt + lam)): # above the melt temperature
            Lambda = lam + (c_melt_s-c_melt_l)*T_melt
            return ((H/rho_l)-Lambda)/c_melt_l
        return T_melt

    # the gap resistance. modeled as linearly decreasing up to T_melt, then zero thereafter
    def R_g_func(self, T, R_cur):
        if (R_cur == 0 or T >= self.P.T_melt):
            return 0
        # R_cur is not zero and T < T_melt
        return self.P.R_g_0+ self.P.k_R_g*(T-self.P.T_ambient)


    # see eqn 3.4
    def R_plates(self):
        R1 = 0
        R2 = 0
        for j in range(0,self.P.n1):
            R1 += (1/self.sigma[j] + 1/self.sigma[j+1])
        R1 *= (self.P.dx1/2)
        for j in range(self.P.n1+1,self.P.N-1):
            R2 += (1/self.sigma[j] + 1/self.sigma[j+1])
        R2 *= (self.P.dx2/2)
        return R1 + R2 + self.R_g

    # see eqn 3.5
    def T_interface(self):
        n1 = self.P.n1
        dx1 = self.P.dx1
        dx2 = self.P.dx2
        dk = (self.k[n1-1]/dx1 + self.k[n1+1]/dx2)
        return (self.T[n1-1]*self.k[n1-1]/dx1 + self.I*self.I*self.R_g + self.T[n1+1]*self.k[n1+1]/dx2)/dk

    # compute the average temperature of the workpiece
    def T_ave(self):
        sum = 0
        for t in self.T:
            sum += t
        return sum/len(self.T)

# solution steps
# 1. given the enthalpies at time i, compute the non-interface enthalpies at time i+1 (eqns 3.2, 2.1, 3.3, 3.4)
# 2. evalutate the interface temperature using eqn 3.5.
# 3. given the temperature at all points at time i+1, go through each position, and:
#   a. note if it is at/above the melt temp (to keep track of nugget size)
#   b. if it is at the melting temp, compute the liquid fraction from eqn 3.6
#   c. compute the phsical constants (k,c,sigma) at each position (for next iteration)

    def advance(self, u):
        # if we are past the final time, let caller know we are done
        if (self.t > self.S.t_final):
            return True

        V_0 = u[0]
        n1 = self.P.n1
        dx1 = self.P.dx1
        dx2 = self.P.dx2

        # get the interface resistance and total resistance, and compute the current
        self.R_g = self.R_g_func(self.T[n1], self.R_g)
        R_p = self.R_plates()
        R_tot = self.P.R_0 + R_p
        self.I = V_0/R_tot
        #print "Rtot = {}, R_0= {}, R_p = {}, R_g = {}".format(R_tot, self.P.R_0, R_p, self.R_g)

        # save temperature history
        if (self.step%self.S.saveRate == 0 and self.step >= self.S.saveMin):
            self.file.write(str(self.T)+"\n")
            self.file3.write(str(self.beta)+"\n")

        # advance the enthalpy and temperature everywhere except interface
        for j in range (1,n1): # portion in plate 1
            F = self.I*V_0/(self.sigma[j])
            dH = self.S.dt*((1/(dx1*dx1))*((self.k[j]+self.k[j+1])*(self.T[j+1]-self.T[j])-(self.k[j-1]+self.k[j])*(self.T[j]-self.T[j-1]))+F)
            if (j==n1-1 and self.step%self.S.saveRate==0 and self.S.verbose):
                print "[{}]".format(self.t)
                #print "[{}] H = {}, dH = {}, T = {}, beta = {}, F = {}, I={}, V_0 = {}".format(self.t, self.H[j], dH, self.T[j], self.beta[j], F, self.I, V_0)
                #print("H = {} dH = {} T = {}".format(H[j], dH, T[j]))
            self.H[j] += dH
            self.T[j] = self.T_func(self.H[j])
        for j in range (n1+1,self.P.N-1): # portion in plate 2
            F = self.I*V_0/self.sigma[j]
            self.H[j] += self.S.dt*(1/(dx2*dx2)*((self.k[j]+self.k[j+1])*(self.T[j+1]-self.T[j])-(self.k[j-1]+self.k[j])*(self.T[j]-self.T[j-1]))+F)
            self.T[j] = self.T_func(self.H[j])

        # 2. evalutate the interface temperature using eqn 3.5.
        self.T[n1] = self.T_interface()

        # 3. given the temperature at all points at time i+1, go through each position, and:
        c_melt_s = self.P.C.Value(self.P.T_melt, 0)
        for j in range (0,self.P.N):
            Tj = self.T[j]
            betaj = self.beta[j]
            #   a. note if it is at/above the melt temp (to keep track of nugget size)
            #   b. if it is at the melting temp, compute the liquid fraction from eqn 3.6
            if (Tj > self.P.T_melt):
                self.nugget[j] = 1
                self.beta[j] = 1
            if (Tj == self.P.T_melt):
                # compute the liquid fraction
                self.beta[j] = (self.H[j]/self.P.rho_s - c_melt_s*self.P.T_melt)/(self.P.lambda_-self.H[j]*(1/self.P.rho_l-1/self.P.rho_s))
                # make sure it's between 0 and 1
                if (betaj > 1):
                    self.beta[j] = 1
                if (betaj < 0):
                    self.beta[j] = 0
            if (Tj < self.P.T_melt):
                self.beta[j] = 0
            # and update the physical parameters at this point
            self.c[j] = self.P.C.Value(Tj, betaj)
            self.k[j] = self.P.K.Value(Tj, betaj)
            self.sigma[j] = self.P.Sigma.Value(Tj, betaj)

            # update the max temps
            if (self.beta[j] > self.maxT[j]):
                self.maxT[j] = self.beta[j]
        self.t += self.S.dt
        self.step += 1

    def finalize(self):
        self.file.close()
        self.file3.close()

        # write out the final nugget information
        file2 = open("nugget.csv","w")
        file2.write(str(self.nugget)+"\n")
        file2.close()

        # write out the max beta information
        file2 = open("max_temps.csv","w")
        file2.write(str(self.maxT)+"\n")
        file2.close()


    def readDataFromFile(self, filename):
        file = open(filename,"r")
        l = file.readline()
        Data = []
        while l != "":
            nl = l.strip()
            nl = nl[1:len(nl)-1]
            ls = nl.split(",")
            a = []
            for l in ls:
                a.append(float(l))
            l = file.readline()
            Data.append(a)
        file.close()
        return Data

    # compute the size of the nugget
    def getNuggetExtent(self):
        nug_start = 0.0
        nug_end = 0.0
        in_nugget = False
        for i in range(len(self.nugget)):
            if (not in_nugget):
                if (self.nugget[i] == 1):
                    in_nugget = True
                    nug_start = self.x[i]
            else: # in_nugget == True
                if (self.nugget[i] == 0):
                    in_nugget = False
                    nug_end = self.x[i]
                    break
        return (nug_end - nug_start)

    def init():
        pass

    # create and display a plot
    def createPlot(self):
        Temps = self.readDataFromFile("temperatures.csv")
        #Nugget = self.readDataFromFile("nugget.csv")
        #Beta = self.readDataFromFile("beta.csv")
        z = [0]*self.P.N # the zero line for plotting
        title = "V = {} kV; pulse duration = {} ms".format(self.P.V_0/1000, self.P.t_V_end*1000)
        f,ax = plt.subplots(1, figsize=[7,5])
        f.suptitle(title)
        ax.axvline(x=self.P.l1, color="black")
        ax.set_ylabel("Temperature history (K)")
        ax.set_xlabel("Position on workpiece (m)")
        Ts = map(list,zip(*Temps))
        ax.plot(self.x,Ts)
        ax.fill_between(self.x,z,[m*200 for m in self.maxT], facecolor = 'orange')
        ax.fill_between(self.x,z,[n*200 for n in self.nugget], facecolor = 'red')
        #ax[2].axvline(x=l1, color="black")
        if (self.S.saveFig):
            plt.savefig(title + ".png")
        plt.show()


    # helper function for animation
    def init(self):
        return self.ln,

    # helper function for animation
    def update(self, frame):
        self.ln.set_ydata(self.Ts[frame])
        return self.ln,

    # create and display an animation of the temperature history
    def createAnimation(self):
        self.Ts = self.readDataFromFile("temperatures.csv")
        print len(self.Ts)
        self.fig, self.ax = plt.subplots()
        T = [300.0]*len(self.x)
        self.ln = plt.plot(self.x, T)
        ani = FuncAnimation(self.fig,self.update,init_func=self.init,frames=range(len(self.Ts)))
        plt.show()
