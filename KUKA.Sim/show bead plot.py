import matplotlib.pyplot as P
import csv
import os
from time import strftime, localtime


path = "C:\\temp\\test.csv"

t = []
x = []
y = []
v = []
b = []
bx = []
cur_x = 0.0
cur_t = 0.0

# get a string for the modification time of the file--to be used in plot title
modTimeString = strftime("%a %d %b %Y %H:%M:%S", localtime(os.path.getmtime(path)))


with open(path,"r") as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        # convert "torch velocity" into position by integrating as we go
        cur_x += (float(row[0])-cur_t)*float(row[2])
        cur_t = float(row[0])
        t.append(cur_t)
        x.append(cur_x)
        v.append(float(row[2]))
        y.append(float(row[1]))
        b.append(float(row[1])/2)
        b.append(-float(row[1])/2)
        bx.append(cur_x)
        bx.append(cur_x)

P.figure(figsize=[13,6])
P.subplot(2,1,1)
P.plot(x,y)
P.title("Weld bead width [" + modTimeString + "]")
#P.xlabel("position on workpiece (m)")
P.ylabel("weld bead width (m)")
P.subplot(2,1,2)
P.plot(x,v)
P.xlabel("position on workpiece (m)")
P.ylabel("torch velocity (m/s)")
P.show()
