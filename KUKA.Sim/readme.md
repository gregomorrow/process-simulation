The file "show bead plot.py" needs to go into C:\temp so it can be found by the python script running in
KUKA.Sim. Note that if we could install matplotlib to the KUKA.Sim python library, we would not need this step,
we could just plot directly from the python script in the KUKA.Sim model.