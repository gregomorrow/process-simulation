import Solver as S
import INEEL_ISU_ArcWeld as model
import matplotlib.pyplot as plt


x0 = [0.1,0.01,0.01,0.01,1]
u0 = [0.3,29,0.015]
s = S.Solver(x0,u0,0.0,0.001,model)

res = s.Simulate(1)

labels = ["voltage","current","droplet displacement","droplet velocity","droplet mass","stick-out"]
fig, ax = plt.subplots()
for i in range(6):
    plt.subplot(2,3,i+1)
    plt.plot(res.T, [y[i] for y in res.Y],label = labels[i])
    plt.title(labels[i])

legend = ax.legend(loc='lower right', shadow=True)
plt.show()
