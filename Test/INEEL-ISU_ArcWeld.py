# INEEL/ISU welding droplet model
# see "Modeling Sensing and Control of Arc Welding Process" S. Ozcelik
#
#
import Solver as S

a = 0.641
b = 1.15
c = 0.196
C_1 = 2.8855e-10
C_2 = 5.22e-10
C_d = 0.44
rho = 0.2821
rho_w = 7860
rho_p = 1.6
K = 2.5
B = 1e-5
r_w = 4.445e-4
L_s = 0.14e-3
R_a = 0.022
R_s = 0.004
V_0 = 15.7
E_a = 1500
U_b = 10.0
gamma = 2

# x1 droplet displacement
# x2 droplet velocity
# x3 droplet mass
# x4 stick-out
# x5 current
# u1 wire-feed speed
# u2 open-circuit voltage V_oc
# u3 contact tip-to-workpiece distance
def f(x,u,t):
    x1 = x[0]
    x2 = x[1]
    x3 = x[2]
    x4 = x[3]
    x5 = x[4]
    u1 = u[0]
    u2 = u[1]
    u3 = u[2]
    xd = S.np.zeros(5)
    CT = u3
    M_r = C_2*rho*x4*x5*x5+C_1*x5
    r_d = math.pow(3.0*x3/(4*math.pi*rho_w),1.0/3.0)
    R_L = rho*(x4+0.5*(x1+r_d))
    F_em = (mu_0*x5*x5/(4*math.pi))*(a/(1+math.exp((b-(r_d/r_w))/c)))
    F_d = (C_d*(r_d*r_d-r_w*r_w)*math.pi*rho_p*U_b*U_b)/2.0
    F_m = M_r*rho_w*u1
    F_g = 9.81*x3
    F_tot = F_em + F_d + F_m + F_g
    xd[0] = x2
    xd[1] = (-K*x1 - B*x2 + F_tot)/x3
    xd[2] = M_r*rho_w
    xd[3] = u1 - M_r/(math.pi*r_w*r_w)
    xd[4] = (1.0/L_s)*(u2-(R_a+R_s+R_L)*x5-V_0-E_a*(CT-x4))
    
    
# y1 arc voltage
# y2 arc current
def g(x,u,t):
    y = S.np.zeros(2)
    y[0] = V_0 + R_a*x[4]+E_a*(u3-x[3])
    y[1] = x[4]
    
    
def e(x,u,t):
    x1 = x[0]
    x2 = x[1]
    x3 = x[2]
    x4 = x[3]
    x5 = x[4]
    u1 = u[0]
    u2 = u[1]
    u3 = u[2]
    CT = u3
    M_r = C_2*rho*x4*x5*x5+C_1*x5
    r_d = math.pow(3.0*x3/(4*math.pi*rho_w),1.0/3.0)
    R_L = rho*(x4+0.5*(x1+r_d))
    F_em = (mu_0*x5*x5/(4*math.pi))*(a/(1+math.exp((b-(r_d/r_w))/c)))
    F_d = (C_d*(r_d*r_d-r_w*r_w)*math.pi*rho_p*U_b*U_b)/2.0
    F_m = M_r*rho_w*u1
    F_g = 9.81*x3
    F_tot = F_em + F_d + F_m + F_g
    if (F_tot > F_s):
        result = True
        # set up the new states
        xnew = S.np.zeros(5)
        xnew[0] = r_d
        xnew[1] = 0
        xnew[2] = (x3/2.0)*(1.0-1.0/(1.0+math.exp(-100.0*x2)))
        xnew[3] = x4
        r_d_new = math.pow(3.0*xnew[2]/(4*math.pi*rho_w),1.0/3.0)
        R_L_new = rho*(x4+0.5*(r_d_new))
        xnew[4] = x5+(u2-V_0-E_a*(u3-x4))/(R_a+R_s+R_L_new)-(u2-V_0-E_a*(u3-x4))/(R_a+R_s+R_L)
    else:
        result = False
        xnew = x
        
    return S.ResetResult(result, xnew)
    
    

    