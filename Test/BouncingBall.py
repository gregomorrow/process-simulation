import Solver as S
def f(x,u,t):
    return [x[1], -9.8]
    
def g(x,u,t):
    return x
    
def e(x,x_old,t):
    thresh = 0.0
    restitution = 0.6
    above = 1e-12
    if ((x[0]-thresh)*(x_old[0]-thresh)<=0):
        result = True
    else:
        result = False
    
    return S.ResetResult(result, [above,-restitution*x[1]])
        
def jac(x,u,t):
    return S.np.array([[0.0,1.0],[0.0,0.0]])