# find the eigenmodes of a circular drum head
# try to solve the eigenproblem: del u = lambda u
import numpy as np
import matplotlib.pyplot as pp
from mpl_toolkits.mplot3d import Axes3D

R = 1
Nx = 30
Ny = 30

xx = np.linspace(0,R,num=Nx)
yy = np.linspace(0,R,num=Ny)

dx = xx[1]-xx[0]
dy = yy[1]-yy[0]

u = []
du = []
X = np.zeros((Nx,Ny))
Y = np.zeros((Nx,Ny))
V = np.zeros((Nx,Ny))

for y in yy:
    du.append([xx[0],y])
    du.append([xx[len(xx)-1],y])
    for x in xx:
        u.append([x,y])

for x in xx:
    du.append([x,yy[0]])
    du.append([x,yy[len(yy)-1]])

d = len(u) # number of unknowns

A = np.zeros((d,d))

c = 1/(dx*dx)
for i in range(d):
    uu = u[i]
    x = uu[0]
    y = uu[1]
    # d2u/dx2
    A[i,i] += -2*c # central part
    if (i-1>=0):
        A[i-1,i] += c
    if (i+1<d):
        A[i+1,i] += c
    # d2u/dy2
    A[i,i] += -2*c
    if (i-Nx>=0):
        A[i, i-Nx] += c
    if (i+Nx<d):
        A[i,i+Nx] += c

ww, vv = np.linalg.eig(A)

# now plot an igenvector
v = vv[-1]

for j in range(Ny):
    for i in range(Nx):
        X[i,j] = xx[i]
        Y[i,j] = yy[j]
        V[i,j] = v[j*Nx+i]

#pp.scatter([uu[0] for uu in u],[uu[1] for uu in u])
#pp.scatter([uu[0] for uu in du],[uu[1] for uu in du])
#pp.matshow(A)
fig = pp.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(X,Y,V)
pp.show()
